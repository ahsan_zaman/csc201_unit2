/*
 * Ahsan Zaman
 * CSC 201 - Unit 2
 * Girl Scout Cookies
 * 
 * Algorithm:
 * 1. Declare two integer arrays counter and scouts
 * 2. Declare and initialize girlScouts to 0
 * 3. Ask user for the number of girls in the troop
 * 4. Input number of girls into girlScouts variable
 * 5. Alocate scouts with the number of girls in the troop
 * 6. Alocate counter with 5
 * 7. Ask user for the cookies sold by each girl scout
 * 8. Increment the counter array for specific stats
 * 9. Display the results in a table
 * END 
 */

import java.util.Scanner;
public class Source {
	public static void main( String[] args ){
		Scanner input = new Scanner(System.in);
		int[] statsCounter, scouts;				// Declaring two arrays
		int girlScouts=0;						// Number of girl scouts in the troop
		
		System.out.println( "Enter total number of girls in the troop: " );
		girlScouts = input.nextInt();
		scouts = new int[girlScouts];
		statsCounter = new int[5];				// for recording which girl scout has sold cookie boxes as per category
		
		for( int i=0; i<girlScouts ;i++ ){
			System.out.print( "Enter cookies sold by Girl Scout #"+(i+1)+": " );
			scouts[i] = input.nextInt();		// Taking input and recording stats
			if( scouts[i]>=0 & scouts[i]<=10 )
				statsCounter[0]++;				// Number of girls who have sold from 0 to 10 boxes 
			else if( scouts[i]>=11 & scouts[i]<=20 )
				statsCounter[1]++;				// Number of girls who have sold from 11 to 20 boxes
			else if( scouts[i]>=21 & scouts[i]<=30 )
				statsCounter[2]++;				// Number of girls who have sold from 21 to 30 boxes
			else if( scouts[i]>=31 & scouts[i]<=40 )
				statsCounter[3]++;				// Number of girls who have sold from 31 to 40 boxes
			else if( scouts[i]>=41 )
				statsCounter[4]++;				//Number of girls who have sold more than 41 boxes
		}
												// Displaying results recorded in the counter array
		System.out.println( "\n\t\t**Results**\nTOTAL BOXES\t\tNUMBER OF GIRL SCOUTS" );
		System.out.println( "0 to 10\t\t\t"+statsCounter[0]+"\n11 to 20\t\t"+statsCounter[1] );
		System.out.println( "21 to 30\t\t"+statsCounter[2]+"\n31 to 40\t\t"+statsCounter[3] );
		System.out.println( "41 or more\t\t"+statsCounter[4] );
		
		input.close();
	}
}
