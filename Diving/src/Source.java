/*
 * Ahsan Zaman
 * CSC 201 - Unit 2
 * Diving (Diver Scoring Program)
 * 
 * Algorithm:
 * 1. Declare float array diverScores 
 * 2. Declare and initialize float degree of Difficulty variable and score variable
 * 3. Ask user for a valid degree of difficulty
 * 4. Get all diver's scores after validating each individual input.
 * 5. Calculate the scores using the given method
 * 6. Round off the score to two decimal places.
 * 7. Display degree of difficulty
 * 8. Display diver's all scores
 * 9. Display diver's score.
 */

import java.util.Scanner;
public class Source {
	public static Scanner input = new Scanner(System.in);
	public static final int JUDGES_SIZE = 7;
	public static void main( String[] args ){
		
		float[] diverScores = new float[JUDGES_SIZE];
		float degDiff=0.0f, score=0.0f;
		System.out.println( "\t\t**Diver Scoring Program**\n " );
																		// Calling all methods and saving into appropriate locations
		degDiff = inputValidDegreeofDifficulty();
		diverScores = inputAllScores();
		score = calculateScore( diverScores, degDiff );
		score = Math.round(score*100)/100;								// To obtain score in 2 decimal points
																		// Displaying results
		System.out.println( "\nDegree of Difficulty for diver: "+degDiff );
		for( int i=0; i<JUDGES_SIZE ;i++ ){
			System.out.println( "Judge #"+(i+1)+" score: "+diverScores[i] );
		}
		System.out.println( "Diver overall score: "+score );
		
		input.close(); 													// To prevent Scanner object from leaking
	}
	
	public static float inputValidScore(){
		float temp=11f;
		while( temp<0 | temp>10 ){										// Loop runs until a valid entry is input
			System.out.println( "Please enter a valid score between 0 and 10: " );
			temp = input.nextFloat();
			if( temp<0 | temp>10 )
				System.out.println( "Invalid entry. Please try again." );
			else System.out.println( "Valid Entry" );
		}
		return temp;
	}
	
	public static float[] inputAllScores(){
		float[] temp = new float[JUDGES_SIZE];
		int count=0;													// Counter
		while( count!=JUDGES_SIZE ){									// Calls method 7 times, to store 7 scores
			temp[count] = inputValidScore();							// Gets input and validates it
			count++;
		}
		return temp;
	}
	
	public static float inputValidDegreeofDifficulty(){
		float temp=0.0f;
		while( temp<1.2 | temp>3.8 ){									// Ensures that value entered is valid
			System.out.println( "Please enter a valid degree of difficulty between 1.2 and 3.8 for diver: " );
			temp = input.nextFloat();
			if( temp<1.2 | temp>3.8 )
				System.out.println( "Invalid entry. Please try again. " );
			else System.out.println( "Valid entry." );
		}
		return temp;
	}
	
	public static float calculateScore( float[] judgeScore, float degDifficulty ){
		float Score=0.0f;
		int maxIndex=0, minIndex=0;
		for( int i=1; i<JUDGES_SIZE ;i++ ){								// Finding out the minimum and maximum values to take out
			if( judgeScore[i]>judgeScore[maxIndex] )
				maxIndex = i;
			else if( judgeScore[i]<judgeScore[minIndex] )
				minIndex = i;
		}
		
		for( int i=0; i<JUDGES_SIZE ;i++ ){
			if( i==maxIndex || i==minIndex ){							// Does nothing in case of min and max value
																		// This is to exclude these values from the overall score
			}
			else Score += judgeScore[i];								// sum of all scores
		}
		Score *= degDifficulty*0.6f;									// Calculating final score
		return Score;
	}
}
